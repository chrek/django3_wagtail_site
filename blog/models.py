from django import forms
from django.db import models

from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase

# Create your models here.

from wagtail.core.models import Page, Orderable
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index

from wagtail.snippets.models import register_snippet


class BlogIndexPage(Page):
    """
    docstring
    """
    intro = RichTextField(blank=True)

    # def get_context(self, request):
    def get_context(self, request, *args, **kwargs):
        """
        docstring
        """
        # Update context to include only published posts, ordered by reverse-chron
        # context = super().get_context(request)
        context = super(BlogIndexPage, self).get_context(
            request, *args, **kwargs)
        blogpages = self.get_children().live().order_by('-first_published_at')
        context['blogpages'] = blogpages
        return context

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full"),
    ]


class BlogTagIndexPage(Page):
    """
    docstring
    """
    # def get_context(self, request):

    def get_context(self, request, *args, **kwargs):
        """
        docstring
        """
        # Filter by tag
        tag = request.GET.get('tag')
        blogpages = BlogPage.objects.filter(tags__name=tag)

        # Update context to include only published posts, ordered by reverse-chron
        # context = super().get_context(request)
        context = super(BlogTagIndexPage, self).get_context(
            request, *args, **kwargs)
        context['blogpages'] = blogpages
        return context


class BlogPageTag(TaggedItemBase):
    """
    docstring
    """
    content_object = ParentalKey(
        'BlogPage',
        related_name='tagged_items',
        on_delete=models.CASCADE
    )


class BlogPage(Page):
    """
    docstring
    """
    date = models.DateField("Post date")
    intro = models.CharField(max_length=250)
    body = RichTextField(blank=True)
    tags = ClusterTaggableManager(through=BlogPageTag, blank=True)
    categories = ParentalManyToManyField('blog.BlogCategory', blank=True)

    """get the image from the first gallery item (or None if no gallery items exist)"""

    def main_image(self):
        """
        docstring
        """
        gallery_item = self.gallery_images.first()
        if gallery_item:
            gallery_item_image = gallery_item.image
        else:
            gallery_item_image = None
        return gallery_item_image

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('date'),
            FieldPanel('tags'),
            FieldPanel('categories', widget=forms.CheckboxSelectMultiple),
        ], heading="Blog information"),
        FieldPanel('intro'),
        # FieldPanel('body', classname="full"),
        FieldPanel('body'),
        InlinePanel('gallery_images', label="Gallery images"),
    ]


class BlogPageGalleryImage(Orderable):
    """
    docstring
    """
    page = ParentalKey(BlogPage, on_delete=models.CASCADE,
                       related_name='gallery_images')
    image = models.ForeignKey('wagtailimages.Image',
                              on_delete=models.CASCADE, related_name='+')
    caption = models.CharField(blank=True, max_length=250)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]


@register_snippet
class BlogCategory(models.Model):
    """
    docstring
    """
    name = ""
    name = models.CharField(max_length=255)
    icon = models.ForeignKey(
        'wagtailimages.Image', null=True, blank=True, on_delete=models.SET_NULL, related_name='+'
    )

    panels = [
        FieldPanel('name'),
        ImageChooserPanel('icon'),
    ]

    def __str__(self):
        """
        docstring
        """
        # return "Blog category is " + self.name
        return self.name

    class Meta:
        """
        docstring
        """
        verbose_name_plural = 'blog categories'
